//
//  ViewController.m
//  USStates
//
//  Created by Orlando Gotera on 10/30/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSArray *states;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.states = @[ @[@"Alabama", @"Montgomery"], @[@"Alaska", @"Juneau"], @[@"Arizona", @"Phoenix"], @[@"Arkansas", @"Little Rock"], @[@"California", @"Sacramento"], @[@"Colorado", @"Denver"], @[@"Connecticut", @"Hartford"], @[@"Delaware", @"Dover"], @[@"Florida", @"Tallahassee"], @[@"Georgia", @"Atlanta"], @[@"Hawaii", @"Honolulu"], @[@"Idaho", @"Boise"], @[@"Illinois", @"Springfield"], @[@"Indiana", @"Indianapolis"], @[@"Iowa", @"Des Moines"], @[@"Kansas", @"Topeka"], @[@"Kentucky", @"Frankfort"], @[@"Louisiana", @"Baton Rouge"], @[@"Maine", @"Augusta"], @[@"Maryland", @"Annapolis"], @[@"Massachusetts", @"Boston"], @[@"Michigan",@"Lansing"],@[@"Minnesota", @"St. Paul"], @[@"Mississippi", @"Jackson"], @[@"Missouri", @"Jefferson"]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.states.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // define the type of the tablecell
    static NSString * simpleTableIdentifier = @"SimpleTableIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:simpleTableIdentifier];
    }
    UIImage * image = [UIImage imageNamed:@"star"];
    cell.imageView.image = image;
    
    UIImage *highlightedImage = [UIImage imageNamed:@"star2"];
    cell.imageView.highlightedImage = highlightedImage;
    
    cell.textLabel.text = self.states[indexPath.row][0];
    cell.detailTextLabel.font = [UIFont italicSystemFontOfSize:15];
    cell.detailTextLabel.text = self.states[indexPath.row][1];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *message = [[NSString alloc]initWithFormat:@"You selected state %@ with capital %@.", self.states[indexPath.row][0], self.states[indexPath.row][1]];
    
    NSLog(@"%@", message);
    
}
    
    
    
    


@end
